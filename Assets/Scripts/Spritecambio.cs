using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spritecambio : MonoBehaviour
{

    [SerializeField]
    Sprite Bird_Blue;
    [SerializeField]
    Sprite Bird_Red;
    [SerializeField]
    Sprite Bird_Yellow;

    private Animator animation;

    public int bird_c = 0;
    // Start is called before the first frame update
    void Awake()
    {

        bird_c = Random.Range(0, 3);
        animation = this.GetComponent<Animator>();
        switch (bird_c)
        {
            case 0:
                animation.SetInteger("Cambio", 0);
                break;
            case 1:
                animation.SetInteger("Cambio", 1);
                break;
            case 2:
                animation.SetInteger("Cambio", 2);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
