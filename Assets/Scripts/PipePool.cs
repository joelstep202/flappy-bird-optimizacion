using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipePool : MonoBehaviour
{

    [SerializeField]
    private GameObject pipepref_d;
    [SerializeField]
    private GameObject pipepref_n;
    [SerializeField]
    private List<GameObject> pipepool;
    [SerializeField]
    private int poolsize = 2;

    public Spritecambio numero;
    private int color;

    private static PipePool instance;

    public static PipePool Instace { get { return instance; } }

    [SerializeField]
    GameObject fondo_Dia;
    [SerializeField]
    GameObject fondo_Noche;

    // Start is called before the first frame update
    void Awake()
    {
        
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        color = Random.Range(0, 2);

        if (color == 0)
        {
            fondo_Dia.SetActive(true);
        }
        else
        {
            fondo_Noche.SetActive(true);
        }

        if (color == 0)
        {
            Add(poolsize);
        }
        if(color == 1)
        {
            Add2(poolsize);
        }
    }

    private void Add(int number)
    {
        for (int i = 0; i < number; i++)
        {
            GameObject pipe = Instantiate(pipepref_d);
            
            pipe.SetActive(false);
            pipepool.Add(pipe);
        }
    }

    private void Add2(int number)
    {
        for (int i = 0; i < number; i++)
        {
            GameObject pipe = Instantiate(pipepref_n);

            pipe.SetActive(false);
            pipepool.Add(pipe);
        }
    }

    public GameObject Pipe()
    {
        for(int i = 0; i < pipepool.Count; i++)
        {
            if(!pipepool[i].activeSelf)
            {
                pipepool[i].SetActive(true);
                return pipepool[i];
            }
        }
        if(color == 0)
        {
            Add(1);
        }
        if(color == 1)
        {
            Add2(1);
        }
        pipepool[pipepool.Count - 1].SetActive(true);
        return pipepool[pipepool.Count - 1];
    }

    public void Color(int number)
    {
        color = number;
    }
}
